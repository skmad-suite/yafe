.. _tutorial_oar:

Using yafe with OAR 
###################

The following code is an example of script suitable to deploy an experiment
on a computer cluster running OAR, thanks to the funtion
:func:`~yafe.utils.generate_oar_script`.

This script can be downloaded
:download:`here <example_oar.py>`.

.. literalinclude:: example_oar.py
